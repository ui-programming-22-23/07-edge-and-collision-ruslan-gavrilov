const canvas = document.getElementById("the_canvas");
const ctx = canvas.getContext("2d");
const WIDTH = canvas.width=  600;
const HEIGHT= canvas.height= 600;
let left_arrow = 37;
let up_arrow = 38;
let right_arrow=39;
let down_arrow=40;


function gameObject(x,y,width,height)
{
   // this.spritesheet = spritesheet;
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
}

let square = new gameObject(30,30,30,30);
let enemySquare = new gameObject(150,150,75,75);

let speed = 5;

function GamerInput(input) {
    this.action = input; // Hold the current input as a string
}

// Default GamerInput is set to None
let gamerInput = new GamerInput("None"); //No Input

window.addEventListener("keydown", inputHandle);


function inputHandle(event)
{
    switch(event.keyCode)
    {
        case right_arrow:
            //console.log("right arrow");
            gamerInput = new GamerInput("Right");
            break;
        case left_arrow:
            gamerInput = new GamerInput("Left");
            break;
        case down_arrow:
            gamerInput = new GamerInput("Down");
            break;
        case up_arrow:
            gamerInput = new GamerInput("Up");
            break;
        default:
            break;

    }
}

function movement()
{
    if (gamerInput.action === "Up") {
        square.y -= speed; // Move Player Up
    } else if (gamerInput.action === "Down") {
        square.y += speed; // Move Player Down
    } else if (gamerInput.action === "Left") {
        square.x -= speed; // Move Player Left
    } else if (gamerInput.action === "Right")
    {
        square.x += speed; // Move Player Right
    }
}

function update()
{
    movement();
    boundary();
    collision(enemySquare);
}

function boundary()
{
    if(square.x<0)
    {
        square.x =0;
    }
    if(square.x>WIDTH - square.width)
    {
        square.x =WIDTH - square.width;
    }
    if(square.y>HEIGHT - square.height)
    {
        square.y =HEIGHT - square.height;
    }
    if(square.y<0)
    {
        square.y = 0;
    }

}

function collision(gameObject)
{
    let xcol= (square.x + square.width > gameObject.x && square.x< enemySquare.x + gameObject.width );
    let ycol = (square.y + square.height > gameObject.y && square.y< gameObject.y + gameObject.height );
    if(xcol && ycol)
    {
        ctx.fillStyle="#05C3DD";
        console.log("Collision");
    }
    else
    {
        ctx.fillStyle="#000000";
    }
}

function draw()
{
    ctx.clearRect(0, 0, WIDTH, HEIGHT);

    ctx.fillRect(square.x,
                 square.y,
                 square.width,
                 square.height);

    ctx.strokeRect(enemySquare.x,
                enemySquare.y,
                enemySquare.width,
                enemySquare.height);
        
   
}

//updates game
function gameloop()
{
    draw();
    update();
    
    window.requestAnimationFrame(gameloop);
}



window.requestAnimationFrame(gameloop); //animate the player